import sys
import time
import os
import shutil
import nibabel
import numpy as np
from stl import mesh
import warp
import pdb
import subprocess
from scipy.ndimage import gaussian_filter
from scipy.ndimage.morphology import binary_dilation
from scipy.ndimage.morphology import binary_closing
from scipy.ndimage.morphology import binary_erosion

import matplotlib.pyplot as plt

def create_plots(input_dir, z, resolution, max_displacement, vx_coords, displacement_normals, original, moving, warped, displacement_fields):
	
	#Generate png figures
	#z is the slice number that you want to create the figure at

	dpi = 300

	i = np.where( vx_coords[:,2]==z )[0]
	vx_coords2d = vx_coords[i,:]
	displacement_normals2d = displacement_normals[i,:]

	img_original = original.get_data()[:,:,z].transpose()
	img_moving = moving.get_data()[:,:,z].transpose()
	img_warped = warped.get_data()[:,:,z].transpose()

	(H,W) = img_original.shape
	figsize = (10, 10 * (H/W))
	mm2inches = resolution*(figsize[0]/W)

	plt.figure(figsize = figsize)
	plt.subplot(2,2,1)
	plt.imshow(img_original, cmap='gray', vmin = -500, vmax=1000 )
	plt.quiver( vx_coords2d[:,0], vx_coords2d[:,1], -displacement_normals2d[:,0], displacement_normals2d[:,1], color='red', scale=(1/mm2inches), scale_units='inches')
	plt.xlabel('Fixed')

	plt.subplot(2,2,2)
	plt.imshow(img_moving, cmap='gray', vmin = -500, vmax=1000 )
	plt.quiver( vx_coords2d[:,0], vx_coords2d[:,1], -displacement_normals2d[:,0], displacement_normals2d[:,1], color='red', scale=(1/mm2inches), scale_units='inches')
	plt.xlabel('Moving')

	[gx,gy] = np.gradient( img_original )
	g_original = np.sqrt( gx**2 + gy**2 )
	g_original = np.sqrt(g_original)
	[gx,gy] = np.gradient( img_moving )
	g_moving = np.sqrt( gx**2 + gy**2 )
	g_moving = np.sqrt(g_moving)
	[gx,gy] = np.gradient( img_warped )
	g_warped = np.sqrt( gx**2 + gy**2 )
	g_warped = np.sqrt(g_warped)

	g_original = g_original / g_original.max()
	g_moving = g_moving / g_moving.max()
	g_warped = g_warped / g_warped.max()


	cimg1 = np.zeros(img_original.shape + (3,))
	cimg1[:,:,0] = g_moving
	cimg1[:,:,2] = g_original

	cimg2 = np.zeros(img_original.shape + (3,))
	cimg2[:,:,0] = g_moving
	cimg2[:,:,2] = g_warped
	
	plt.subplot(2,2,3)
	plt.imshow(cimg1, vmax=.2)
	plt.xlabel('Fused - Fixed and Synthetic')

	plt.subplot(2,2,4)
	plt.imshow(cimg2, vmax=.2)
	plt.xlabel('Fused - Fixed and Warped')
	#plt.gca().invert_yaxis()
	
	plt.savefig( os.path.join(input_dir, 'synthetic_deformation.png'), dpi=dpi)
	plt.close()


def create_deformation_nifti( output_file, 
							  template_image, 
							  vx_coords, 
							  displacement_vectors, 
							  max_displacement, 
							  deformation_blur ):
	""" This function will create a deformation field NIFTI file and return the displacement fields and vectors
	Arguments:
		output_file: 'str' The deformation field output file
		template_image: 'nibabel.Nifti1Image' a nifti image that should have the same size and spatial metadata as the deformation field image
	"""

	# Create the displacement field image data. Needs to be a 5-d array as per the NIFTI file specs 
	# (the 4th dimension is reserved for time)
	displacement_field = np.zeros( template_image.shape + (1,3) ).astype('float32')
	for k in range(3):
		displacement_field[vx_coords[:,0], vx_coords[:,1], vx_coords[:,2], 0, k] = displacement_vectors[:,k]


	# Apply a gaussian blur to each displacement field
	for k in range(3):
		displacement_field[:,:,:,0,k] = gaussian_filter(displacement_field[:,:,:,0,k], deformation_blur)


	# Compute the magnitude of the displacement field at every voxel after blurring
	disp_field_magnitude = np.sqrt( ( displacement_field[:,:,:,0,:]**2 ).sum(axis=3) )
	current_max_disp = disp_field_magnitude.max()

	# Normalized the displacement field so that the maximal displacement is equal to 
	displacement_field = (max_displacement/current_max_disp) * displacement_field
	displacement_field = displacement_field.astype('float32')

	## Create the output nifti image with header components that mimic the output header
	## from the elastix
	T = template_image.get_sform()
	hdr = nibabel.Nifti1Header()
	hdr.set_data_dtype( displacement_field.dtype )
	hdr.set_intent( 1007 )
	hdr.set_qform( T, code=1 )
	nifti = nibabel.Nifti1Image( displacement_field, T, hdr )
	nifti.to_filename( output_file )
	#######

	for k in range(3):
		displacement_vectors[:,k] = displacement_field[vx_coords[:,0], vx_coords[:,1], vx_coords[:,2], 0, k]

	return displacement_field, displacement_vectors


def gauss_func(x, sigma):
	# This is a just gaussian function, but the maximal value is 1
	return np.exp( (x**2) / (-2*(sigma**2)) )

def main():
	run_registration( 'example_image', 
					  deformation_voxel = [77,27,350,1], 
					  max_displacement= 2.5,
					  deformation_sigma = 5.0, 
					  deformation_blur = 10.0 )

def run_registration( input_dir, deformation_voxel, max_displacement, deformation_sigma, deformation_blur, use_gradient_magnitude_image=True ):
	""" This function performs the entire synthetic deformation experiment workflow 

	Arguments:
		input_dir: 'str' The input directory that files are located in. 
					The input direcory, must have a mesh.stl file, and a image.nii.gz file
		deformation_voxel: 'list'. The voxel coordinate where the deformation will be centered
		max_displacement: 'float' The amount of maximal displacement desired, which will occur at the deformation_voxel
		deformation_sigma: 'float' The sigma value in the gaussian function that controls how 
							fast the deformation magnitude decays as the euclidean distance from the deformation_voxel increases
		use_gradient_magnitude_image: 'bool' Use the gradient magnitude image for registration if True. If false, use the normal image
	"""


	start_time = time.time()

	mesh_file = os.path.join( input_dir, 'mesh.stl')

	original_image_file = os.path.join( input_dir, 'image.nii.gz' )
	original_mask_image_file = os.path.join( input_dir, 'image_VOI.nii.gz' )
	original_image_gradient_file = os.path.join( input_dir, 'image_gradient.nii.gz' )
	original_dilated_mask_image_file = os.path.join( input_dir, 'image_VOI_dilated.nii.gz' )

	synthetic_image_file = os.path.join( input_dir, 'synthetic.nii.gz' )
	synthetic_mask_image_file = os.path.join( input_dir, 'synthetic_VOI.nii.gz' )
	synthetic_image_gradient_file = os.path.join( input_dir, 'synthetic_gradient.nii.gz' )
	synthetic_dilated_mask_image_file = os.path.join( input_dir, 'synthetic_VOI_dilated.nii.gz' )

	warped_image_file = os.path.join( input_dir, 'warped.nii.gz' )

	displacement_field_file_name = os.path.join( input_dir, 'true_displacement.nii.gz' )
	estimated_displacement_field_file_name = os.path.join( input_dir, 'estimated_displacement.nii.gz' )
	displacement_difference_image_file_name = ( os.path.join( input_dir, 'displacement_error.nii.gz' ))

	border_image_file_name = os.path.join( input_dir, 'original_border.nii.gz' )
	synthetic_border_image_file_name = os.path.join( input_dir, 'synthetic_border.nii.gz' )
	
	logfile = os.path.join( input_dir, 'log.txt' )

	elastix_binary_folder = 'C:\Elastix' if (os.name == 'nt') else '/elastix/bin'
	elastix_output_folder = os.path.join(input_dir, 'output_elastix')
	transformix_output_folder = os.path.join(input_dir, 'output_transformix')

	#Delete any old elastix and transformix output
	if os.path.isdir( elastix_output_folder ):
		shutil.rmtree(elastix_output_folder)
	os.mkdir(elastix_output_folder)
	if os.path.isdir( transformix_output_folder ):
		shutil.rmtree(transformix_output_folder)
	os.mkdir(transformix_output_folder)


	aorta_mesh = mesh.Mesh.from_file( mesh_file )

	#Read the CT image
	original = nibabel.load(original_image_file)

	#These are 4 x 4 matrices that convert voxcel coords to physical coords and then back again
	T = original.get_sform()  
	iT = np.linalg.inv(T)

	#The in-plane image resolution
	resolution = abs( T[0,0] )
	
	#Get the STL file points, which are in physical coordinates
	border_points = aorta_mesh.vectors.mean(1)
	border_points = np.insert( border_points, 3, 1.0, axis=1) #This adds a column of 1's
	vx_coords = (iT @ border_points.transpose()).transpose()
	vx_coords = np.round(vx_coords).astype(int)[:,:3]

	########## Create border image ##############
	borders = original.get_data().copy()
	borders[:] = 0
	borders[vx_coords[:,0], vx_coords[:,1], vx_coords[:,2]] = 1
	borders = borders.astype('uint8')
	nifti = nibabel.Nifti1Image(borders, T)
	# Set nifti data dtype same as img
	nifti.set_data_dtype(borders.dtype)
	nifti.to_filename( border_image_file_name )
	#####

	# N is the number of points in the mesh model
	N = vx_coords.shape[0]

	#Get the physical coordinate of the deformation voxel
	deformation_point = np.expand_dims( np.array( deformation_voxel ), 0)
	deformation_point = ( T @ deformation_point.transpose() ).transpose()[:,:3]
	#Now copy that to N rows. This makes deformation_point go from size (1,3) to (N,3)
	deformation_point = np.tile(deformation_point,(N,1))

	#Compute the euclidean distance between all of the border points and the deformation points
	border_points = border_points[:,:3]
	distances = (border_points - deformation_point)**2
	distances = np.sqrt( distances.sum(axis=1) )

	# This finds the index of the points with the smallest distance, which is the deformation point itself.
	# So j in the index for the deformation points
	j = np.argmin(distances)

	#Get the unit normals
	unit_normals = aorta_mesh.normals

	#Compute the magnitude of the normals
	normal_magnitudes = np.sqrt( (unit_normals**2).sum(axis=1) )

	# Make this an N x 3 matrix
	normal_magnitudes = np.expand_dims(normal_magnitudes,1)
	normal_magnitudes = np.tile(normal_magnitudes,3)

	#Now make the unit normals all have a magnitude of 1.0
	unit_normals = unit_normals / normal_magnitudes

	#The displacement magnitudes is a gaussian function of the distance from a point on the aorta
	displacement_magnitudes = gauss_func( distances, deformation_sigma )
	displacement_magnitudes = np.tile(np.expand_dims(displacement_magnitudes,1),3)

	#Now the displacement vectors at the border of the aorta have the desired magnitudes
	displacement_normals = max_displacement * (unit_normals * displacement_magnitudes)

	synthetic_border_points = border_points[:,:3] + displacement_normals
	synthetic_border_points = np.insert(synthetic_border_points,3,1.0, axis=1)
	synthetic_vx_coords = (iT @ synthetic_border_points.transpose()).transpose()
	synthetic_vx_coords = np.round(synthetic_vx_coords).astype(int)[:,:3]

	### Write out and image of the borders for the synthetic image
	synthetic_borders = original.get_data().copy()
	synthetic_borders[:] = 0
	synthetic_borders[synthetic_vx_coords[:,0], synthetic_vx_coords[:,1], synthetic_vx_coords[:,2]] = 1
	synthetic_borders = binary_closing(synthetic_borders,iterations=7)
	synthetic_borders = synthetic_borders.astype('uint8')
	nifti = nibabel.Nifti1Image(synthetic_borders, T)
	nifti.set_data_dtype(synthetic_borders.dtype)
	nifti.to_filename( synthetic_border_image_file_name )
	##############################################

	print('Creating the deformation field...')
	displacement_fields, displacement_normals = create_deformation_nifti( displacement_field_file_name, 
																		  original, synthetic_vx_coords, displacement_normals, 
																		  max_displacement=max_displacement, 
																		  deformation_blur=deformation_blur )

	#Warp the original images to create the synthetic image
	print('Applying warp...')
	warp.warp_image( original_image_file, 
					 displacement_field_file_name, 
					 synthetic_image_file)


	original_mask = nibabel.load( original_mask_image_file )
	hdr = original_mask.header

	#This creates the loop mask
	original_mask_data = binary_dilation( original_mask.get_data(), iterations=5).astype('int16') - binary_erosion( original_mask.get_data(), iterations=5).astype('int16')

	nifti = nibabel.Nifti1Image( original_mask_data, affine=None, header=hdr )
	nifti.to_filename( original_dilated_mask_image_file )

	#Now warp the dilated loop mask and the original mask
	warp.warp_image( original_dilated_mask_image_file, 
					 displacement_field_file_name, 
					 synthetic_dilated_mask_image_file)

	warp.warp_image( original_mask_image_file, 
					 displacement_field_file_name, 
					 synthetic_mask_image_file)

	
	## This section creates the gradient magnitude images
	[gx,gy,gz] = np.gradient( original.get_data() )
	g = np.sqrt( gx**2 + gy**2 + gz**2 )
	g = 1000*g/g.max()
	hdr = original.header
	nifti = nibabel.Nifti1Image( g, affine=None, header=hdr )
	nifti.to_filename( original_image_gradient_file )
	
	synthetic = nibabel.load( synthetic_image_file )
	[gx,gy,gz] = np.gradient( synthetic.get_data() )
	g = np.sqrt( gx**2 + gy**2 + gz**2)
	g = 1000.0*g/g.max()
	hdr = synthetic.header
	nifti = nibabel.Nifti1Image( g, affine=None, header=hdr )
	nifti.to_filename( synthetic_image_gradient_file )
	#################################

	synthetic = nibabel.load(synthetic_image_file)

	if use_gradient_magnitude_image:
		fixed_image_file = synthetic_image_gradient_file
		moving_image_file = original_image_gradient_file
	else:
		fixed_image_file = synthetic_image_file
		moving_image_file = original_image_file

	elastix_command = ['%s/elastix' % elastix_binary_folder,
					   '-m', moving_image_file, '-f', fixed_image_file,
					   '-mMask', original_dilated_mask_image_file, '-fMask', synthetic_dilated_mask_image_file,
					   '-out', elastix_output_folder,
					   '-p', 'par_nonrigid_NCC.txt']

	transformix_command = ['%s/transformix' % elastix_binary_folder,
					   	   '-def', 'all',
					       '-in', moving_image_file,
					       '-out', transformix_output_folder,
					       '-tp', os.path.join( elastix_output_folder, 'TransformParameters.0.txt')]
					   

	#Print out the elastix command so we can copy it in case we want to run it directly from the command line
	print(' '.join(elastix_command) )
	print('Running registration')

	#This is how you call processes in python
	process = subprocess.run( elastix_command, stdout=subprocess.PIPE)
	os.rename(  os.path.join(elastix_output_folder, 'elastix.log'), logfile)

	#If there is no error
	if process.returncode == 0:

		#Run tranformix
		process2 = subprocess.run( transformix_command, stdout=subprocess.PIPE)

		#If there is no error
		if process2.returncode == 0:

			#Move the deformation field
			os.rename( os.path.join(transformix_output_folder, 'deformationField.nii.gz'), 
				   	   estimated_displacement_field_file_name )

			#Load the estimated displacement
			estimated_displacement = nibabel.load( estimated_displacement_field_file_name )
			estimated_displacement_data = estimated_displacement.get_data()

			#Compute the displacement error magnitude
			displacement_error = estimated_displacement_data - displacement_fields
			displacement_error = np.squeeze(displacement_error)
			displacement_error = np.sqrt( (displacement_error**2).sum(axis=3) )

			#These are the indices of points within 1 cm (5mm radius) of the deformation voxel
			jj = (distances<5)
			error_magnitude = displacement_error[ synthetic_vx_coords[jj,0], synthetic_vx_coords[jj,1], synthetic_vx_coords[jj,2] ]

			### This will create th displacement error image. Not that the values are scaled shifted and rounded. 
			##  This is so that they can be displayed in the ITK-SNAP 3D viewer easily.
			synthetic_borders = binary_dilation(synthetic_borders,iterations=1)
			displacement_error = displacement_error * synthetic_borders.astype('float32')
			displacement_error = np.round(10*displacement_error)
			displacement_error[synthetic_borders>0]+=1.0
			displacement_error[displacement_error>100] = 100
			displacement_error = displacement_error.astype('uint8')
			nifti = nibabel.Nifti1Image(displacement_error, T)
			# Set nifti data dtype same as img
			nifti.set_data_dtype(displacement_error.dtype)
			nifti.to_filename( displacement_difference_image_file_name )
			
			#Print out the median and 95th percentile
			print( np.percentile(error_magnitude, 50) )
			print( np.percentile(error_magnitude, 95) )

			#
			os.rename( os.path.join(transformix_output_folder, 'result.nii.gz'), warped_image_file )
			warped = nibabel.load( warped_image_file )
			print('Creating plots')

			z = deformation_voxel[2]
			#Create the 2D visualization plots
			create_plots( input_dir, z, resolution, max_displacement, vx_coords, displacement_normals, original, synthetic, warped, displacement_fields)


		else:
			print('transformix error!')
			exit()
	else:
		print('elastix error!')
		exit()

	print( (time.time() - start_time)/60.0 )
	print('Finished')


if __name__ == '__main__':
	main()
	sys.stdout.write('\a')
	sys.stdout.flush()