#!/usr/bin/env python
import itk
import pdb


def warp_segmentation( input_file, displacement_file, output_file):
	Dimension = 3

	VectorComponentType = itk.F
	VectorPixelType = itk.Vector[VectorComponentType, Dimension]

	DisplacementFieldType = itk.Image[VectorPixelType, Dimension]

	PixelType = itk.F
	ImageType = itk.Image[PixelType, Dimension]

	reader = itk.ImageFileReader[ImageType].New()
	reader.SetFileName(input_file)

	fieldReader = itk.ImageFileReader[DisplacementFieldType].New()
	fieldReader.SetFileName(displacement_file)
	fieldReader.Update()

	deformationField = fieldReader.GetOutput()

	warpFilter = itk.WarpImageFilter[ImageType, ImageType, DisplacementFieldType].New()

	interpolator = itk.LinearInterpolateImageFunction[ImageType, itk.D].New()

	warpFilter.SetInterpolator(interpolator)

	warpFilter.SetOutputSpacing(deformationField.GetSpacing())
	warpFilter.SetOutputOrigin(deformationField.GetOrigin())
	warpFilter.SetOutputDirection(deformationField.GetDirection())

	warpFilter.SetDisplacementField(deformationField)

	warpFilter.SetInput(reader.GetOutput())

	writer = itk.ImageFileWriter[ImageType].New()
	writer.SetInput(warpFilter.GetOutput())
	writer.SetFileName(output_file)

	writer.Update()

def warp_image( input_file, displacement_file, output_file):
	Dimension = 3

	VectorComponentType = itk.F
	VectorPixelType = itk.Vector[VectorComponentType, Dimension]

	DisplacementFieldType = itk.Image[VectorPixelType, Dimension]

	PixelType = itk.F
	ImageType = itk.Image[PixelType, Dimension]

	reader = itk.ImageFileReader[ImageType].New()
	reader.SetFileName(input_file)

	fieldReader = itk.ImageFileReader[DisplacementFieldType].New()
	fieldReader.SetFileName(displacement_file)
	fieldReader.Update()

	deformationField = fieldReader.GetOutput()

	warpFilter = itk.WarpImageFilter[ImageType, ImageType, DisplacementFieldType].New()

	interpolator = itk.LinearInterpolateImageFunction[ImageType, itk.D].New()

	warpFilter.SetInterpolator(interpolator)

	warpFilter.SetOutputSpacing(deformationField.GetSpacing())
	warpFilter.SetOutputOrigin(deformationField.GetOrigin())
	warpFilter.SetOutputDirection(deformationField.GetDirection())

	warpFilter.SetDisplacementField(deformationField)

	warpFilter.SetInput(reader.GetOutput())

	writer = itk.ImageFileWriter[ImageType].New()
	writer.SetInput(warpFilter.GetOutput())
	writer.SetFileName(output_file)

	writer.Update()
	print('Done')