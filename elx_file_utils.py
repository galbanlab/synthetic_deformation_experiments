# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 11:14:03 2019

@author: kmbanas

Utility functions for reading/writing Elastix parameter and output files

"""

import numpy as np
import re
import json
import os

# reads the column of data with title col_label from the Elastix output file stored at file_path
# returns a list of floats, which are the values in the column below the title
def get_col(file_path, col_label):
    fp = open(file_path, 'r')
    words = [line.split() for line in fp.readlines()]
    words_np = np.array(words)
    col_idx = np.where(words_np[0,:] == col_label)[0].item()
    data = words_np[1:, col_idx]
    fp.close()
    return[float(f) for f in data]
    
# reads an Elastix parameter file and creates the corresponding JSON
# returns a dictionary of Elastix parameters to their values, and dumps this dictionary to a JSON file
# user may specify a filename for the JSON to be written to; if not, the filename is the same as the input file, with the extension changed to .json
#
# once created, the JSON file can simply be read with json.load(fp), where fp points to the file
def read_elx_params(in_file_path, out_file_path=''):
    fp = open(in_file_path, 'r')
    text = fp.read()
    params = re.findall(r'\((.*?)\)', text)
    words = [param.split() for param in params]
    keys = [w[0] for w in words]
    vals = [w[1:] for w in words]
    vals = [[str_to_data_type(string) for string in l] for l in vals] # strip quotes and convert numeric strings to numbers
    vals = [(l[0] if (len(l) == 1) else l) for l in vals]
    dictionary = dict(zip(keys, vals))
    fp.close()
    
    # write the dictionary of parameters and values to a JSON file
    out_file_path = (os.path.splitext(in_file_path)[0] + '.json') if (out_file_path == '') else out_file_path
    fp_w = open(out_file_path, 'w')
    json.dump(dictionary, fp_w)
    fp_w.close()
    return dictionary

# given a dictionary mapping Elastix parameters to their values, it writes the corresponding Elastix parameter file in the format accepted by Elastix
def write_elx_params(elx_param_dict, out_file_path):
    fp = open(out_file_path, 'w')
    for k, v in elx_param_dict.items():
      fp.write('(' + k + ' ' + print_value(v) + ')\n')  
    fp.close()    
    
# returns the given string converted to the proper data type
#   (converts to a number if the string is numeric; otherwise it returns the string itself, stripped of leading and trailing double quotation marks)
#   (converts to a float if it has a decimal point '.'; otherwise converts to an int)
def str_to_data_type(string):
    try:
        if('.' in string):
            return float(string)
        else:
            return int(string)
    except:
        return string.strip(r'\"')
    
def print_value(val):
    print_single = lambda item : str(item) if (not isinstance(item, str)) else ('"' + item + '"')
    return print_single(val) if (not isinstance(val, list)) else ' '.join([print_single(item) for item in val])
        